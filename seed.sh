#!/bin/bash
emacs --batch --no-init-file -l export.el origin.org --funcall org-html-export-to-html

files=(andromeda cygnus dragon pegasus phoenix)

for file in "${files[@]}"
do
  cp -f origin.html "./$file.html"
  if [ "$GITLAB_CI" = "true" ]; then
    perl -i -pe 's~</head>~<link rel="stylesheet" type="text/css" href="./'$file'.css">\n</head>~' "$file.html"    
  else
    perl -i -pe 's~</head>~<link rel="stylesheet" type="text/css" href="./'$file'.scss">\n</head>~' "$file.html"
  fi
  echo "finish $file ..."
done