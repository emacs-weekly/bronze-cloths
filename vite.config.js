import { resolve } from 'path';
import { defineConfig } from 'vite';

export default defineConfig({
    build: {
        cssCodeSplit: true,
        lib: {
          entry: [
            resolve(__dirname, 'style.scss'), 
            resolve(__dirname, 'andromeda.scss'),
            resolve(__dirname, 'cygnus.scss'),
            resolve(__dirname, 'dragon.scss'),
            resolve(__dirname, 'pegasus.scss'),
            resolve(__dirname, 'phoenix.scss'),
          ],
        },
      },
})